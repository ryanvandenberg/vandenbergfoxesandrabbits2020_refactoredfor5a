package far2020;

import java.util.List;
import java.util.Random;

/**
 * This is an animal. There should be more specific documentation here but I
 * wanted to get this out to you. Update this with better documentation.
 * 
 * @author Charles Cusack, 1/23/2020
 *
 */
public abstract class Animal {

	/**
	 * Do whatever an animal is supposed to do each turn. Add any offspring or
	 * other new animals to the newAnimals list.
	 * 
	 * @param newAnimals
	 */
	public abstract void act(List<Animal> newAnimals);

	/**
	 * 
	 * @return the maximum number of offspring the animal can have.
	 */
	public abstract int getMaxLitterSize();

	/**
	 * 
	 * @return the probability that this animal will breed.
	 */
	public abstract double getBreedingProbability();

	/**
	 * 
	 * @return the maximum age this animal can be before it dies.
	 */
	public abstract int getMaxAge();

	/**
	 *
	 * @return the minimum age this animal must be before it can breed.
	 */
	public abstract int getBreedingAge();

	/**
	 * 
	 * This method should be overriden with just one line like this:
	 * 
	 * return new Rabbit(randomAge, field, location);
	 * 
	 * where Rabbit is replaced with whatever the type of the subclass is.
	 * 
	 * @param randomAge
	 *            Should the age be determined randomly? Else it will be set to
	 *            0.
	 * @param field
	 *            The field to place the animal on.
	 * @param location
	 *            The location on the field to place the animal.
	 * @return A new animal of the given type
	 */
	public abstract Animal makeNewAnimal(boolean randomAge, Field field, Location location);

	// ----------------------------------------------------------------------------------
	protected static final Random rand = Randomizer.getRandom();

	private int age;
	private boolean alive;
	private Location location;
	protected Field field;

	public Animal(boolean randomAge, Field field, Location location) {
		age = 0;
		alive = true;
		this.field = field;
		setLocation(location);
		if (randomAge) {
			age = rand.nextInt(getMaxAge());
		}
	}

	/**
	 * Check whether the rabbit is alive or not.
	 * 
	 * @return true if the rabbit is still alive.
	 */
	public boolean isAlive() {
		return alive;
	}

	/**
	 * Indicate that the rabbit is no longer alive. It is removed from the
	 * field.
	 */
	public void setDead() {
		alive = false;
		if (location != null) {
			field.clear(location);
			location = null;
			field = null;
		}
	}

	/**
	 * Return the rabbit's location.
	 * 
	 * @return The rabbit's location.
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * Place the rabbit at the new location in the given field.
	 * 
	 * @param newLocation
	 *            The rabbit's new location.
	 */
	protected void setLocation(Location newLocation) {
		if (location != null) {
			field.clear(location);
		}
		location = newLocation;
		field.place(this, newLocation);
	}

	/**
	 * Increase the age. This could result in the rabbit's death.
	 */
	protected void incrementAge() {
		age++;
		if (age > getMaxAge()) {
			setDead();
		}
	}

	/**
	 * Generate a number representing the number of births, if it can breed.
	 * 
	 * @return The number of births (may be zero).
	 */
	protected int breed() {
		int births = 0;
		if (canBreed() && rand.nextDouble() <= getBreedingProbability()) {
			births = rand.nextInt(getMaxLitterSize()) + 1;
		}
		return births;
	}

	/**
	 * A rabbit can breed if it has reached the breeding age.
	 * 
	 * @return true if the rabbit can breed, false otherwise.
	 */
	protected boolean canBreed() {
		return age >= getBreedingAge();
	}

	/**
	 * Check whether or not this fox is to give birth at this step. New births
	 * will be made into free adjacent locations.
	 * 
	 * @param newAnimals
	 *            A list to return newly born foxes.
	 */
	protected void giveBirth(List<Animal> newAnimals) {
		// New animals are born into adjacent locations.
		// Get a list of adjacent free locations.
		List<Location> free = field.getFreeAdjacentLocations(location);
		int births = breed();
		for (int b = 0; b < births && free.size() > 0; b++) {
			Location loc = free.remove(0);
			Animal animal = makeNewAnimal(false, field, loc);
			newAnimals.add(animal);
		}
	}
}